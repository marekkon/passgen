import pytest
import string
from passgen import get_sequence


def test_get_ascii_lowercase_sequence():
    result = string.ascii_lowercase
    params = {
        'lower': True,
        'upper': False,
        'digits': False,
        'special': False
    }

    assert result == get_sequence(**params)


def test_get_digits_sequence():
    result = string.digits
    params = {
        'lower': False,
        'upper': False,
        'digits': True,
        'special': False
    }

    assert result == get_sequence(**params)


def test_get_special_chars_sequence():
    result = string.punctuation
    params = {
        'special': True
    }

    assert result == get_sequence(**params)


def test_get_all_ascii_sequence():
    result = string.ascii_uppercase + string.ascii_lowercase
    params = {
        'lower': True,
        'upper': True,
    }

    seq = get_sequence(**params)
    for c in result:
        assert c in seq


def test_no_selected_sequence_types():
    params = {
        'lower': False,
        'upper': False,
        'digits': False,
        'special': False
    }

    with pytest.raises(TypeError):
        get_sequence(**params)


def test_missing_sequence_types():
    with pytest.raises(TypeError):
        get_sequence()
