import pytest
import string
from passgen.cli import main


def test_custom_chars_and_length(capsys):
    main(['-c', 'b', '-n', '20'])
    captured = capsys.readouterr()
    assert captured.out == 'bbbbbbbbbbbbbbbbbbbb\n'


def test_output_password_with_all_chars(capsys):
    sequence = string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation
    main(['-a'])
    captured = capsys.readouterr()
    output_password = captured.out.rstrip()
    for char in output_password:
        assert char in sequence


# For this test to pass, pytest must be run without arguments
def test_missing_arguments(capsys):
    with pytest.raises(SystemExit):
        main()
        captured = capsys.readouterr()
        assert captured.err == 'At least one type of characters must be set to get the sequence.'
